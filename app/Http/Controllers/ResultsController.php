<?php

namespace App\Http\Controllers;

use App\Models\Program;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function PHPSTORM_META\map;

class ResultsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $request_staff_id = !empty(request('staff_id')) ? request('staff_id') : auth()->id();

        $StaffID = auth()->user()->role == 'admin' ? $request_staff_id : auth()->id();

        $ProgramID = request('program_id');

        $Programs = $this->getAllPrograms();

        $Results = $this->getResults($StaffID, $ProgramID);

        $Staffs = $this->getAllStaffs();

        return view('Results.index')
            ->with(compact('Results'))
            ->with(compact('Programs'))
            ->with(compact('Staffs'));
    }

    private function getAllStaffs()
    {
        return DB::table('users')->select('id', 'name')->get();
    }

    private function getAllPrograms()
    {
        return Program::all();
    }

    private function getResults($StaffID, $ProgramID)
    {
        return DB::select("select indicators.id ,title, sum(score) as 'total_score'
 from evaluations inner join indicators on evaluations.indicator_id = indicators.id
 where evaluated_id = ? and program_id = ?
 group by indicators.id
 order by indicators.id", [$StaffID, $ProgramID]);
    }

}
