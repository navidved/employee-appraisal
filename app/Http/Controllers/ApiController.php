<?php

namespace App\Http\Controllers;

use App\Models\Indicator;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getIndicators()
    {
        $Indicators = Indicator::all(['id', 'title', 'description']);

        return response()->json($Indicators, 200);
    }
}
