<?php

namespace App\Http\Controllers;

use App\Models\Program;
use App\User;
use Illuminate\Http\Request;

class SelectProgramStaffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $ActivePrograms = $this->getActivePrograms();

        $Staffs = $this->getStaffs();

        return view('SelectProgramStaff.index')
            ->with(compact('ActivePrograms'))
            ->with(compact('Staffs'));
    }

    private function getActivePrograms()
    {
        return Program::where('status', 'active')->get();
    }

    private function getStaffs()
    {
        return User::all();
    }

}
