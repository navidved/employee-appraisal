<?php

namespace App\Http\Controllers;

use App\Models\Evaluation;
use App\Models\Indicator;
use App\Models\Program;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StaffEvaluationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $ProgramID = request('program_id');
        $StaffID = request('staff_id');
        $UserID = auth()->id();

        $ProgramInfo = $this->getProgramInfo($ProgramID);

        $StaffInfo = $this->getStaffInfo($StaffID);

        $IndicatorsWithAnswers = $this->getIndicatorsWithAnswers($ProgramID, $UserID, $StaffID);

        return view('StaffEvaluation.index')
            ->with(compact('ProgramInfo'))
            ->with(compact('StaffInfo'))
            ->with(compact('IndicatorsWithAnswers'));

    }

    public function store()
    {
        $SelectedOption = !empty(request('selected_option')) ? request('selected_option') : 1;

        $attributes['program_id'] = request('program_id');
        $attributes['evaluated_id'] = request('staff_id');
        $attributes['indicator_id'] = request('indicator_id');
        $attributes['evaluator_id'] = auth()->id();
        $attributes['selected_option'] = $SelectedOption;

        $attributes['score'] = $this->getIndicatorScore(request('indicator_id'), $SelectedOption);

        Evaluation::create($attributes);

        return back();
    }

    private function getProgramInfo($ProgramID)
    {
        return Program::where('status', '=', 'active')->findOrFail($ProgramID);
    }

    private function getStaffInfo($StaffID)
    {
        return User::findOrFail($StaffID);
    }

    private function getIndicatorScore($IndicatorID, $SelectedOption)
    {
        if ($SelectedOption > 0 && $SelectedOption <= 4) {

            $Option = 'score_option_' . $SelectedOption;

            return Indicator::findOrFail($IndicatorID)->$Option;

        } else {
            return 0;
        }

    }

    private function getIndicatorsWithAnswers($ProgramID, $UserID, $StaffID)
    {
        if (auth()->user()->role == 'admin') {
            return DB::select('select * from indicators where id not in
              (select indicators.id from indicators left join evaluations on evaluations.indicator_id = indicators.id 
              where program_id = ? and evaluator_id = ? and evaluated_id = ?)', [$ProgramID, $UserID, $StaffID]);
        } else {
            return DB::select('select * from indicators where id not in
              (select indicators.id from indicators left join evaluations on evaluations.indicator_id = indicators.id 
              where program_id = ? and evaluator_id = ? and evaluated_id = ?) and user_access = ?', [$ProgramID, $UserID, $StaffID, 'all']);
        }


    }

}
