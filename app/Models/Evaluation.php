<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Evaluation extends Model
{
    protected $fillable = ['program_id', 'evaluated_id', 'indicator_id', 'evaluator_id', 'selected_option', 'score'];
}
