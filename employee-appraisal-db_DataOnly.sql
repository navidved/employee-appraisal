-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 14, 2019 at 06:17 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employee-appraisal-db`
--

--
-- Dumping data for table `indicators`
--

INSERT INTO `indicators` (`id`, `title`, `description`, `option_1`, `option_2`, `option_3`, `option_4`, `score_option_1`, `score_option_2`, `score_option_3`, `score_option_4`, `user_access`, `created_at`, `updated_at`) VALUES
(1, 'Works to Full Potential', 'In philosophy and bioethics, potential (future) person (in plural, sometimes termed potential people) has been defined as an entity which is not currently a person but which is capable of developing into a person, given certain biologically and/or technically possible conditions.', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(2, 'Quality of Work	', 'Work quality is the value of work delivered by an individual, team or organization. This can include the quality of task completion, interactions and deliverables. Work quality is a common consideration in managing the performance of programs, projects, vendors and individuals.', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(3, 'Work Consistency', 'Logically ordered and/or following the same pattern. For example, a salesperson\'s growth is usually consistent with his or her company\'s earnings. 2. Unchanging; steady. For example, a person that arrives exactly 5 minutes early to work every day is consistently punctual or on-time.', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(4, 'Communication', 'Employee communication by definition in the business world, involves the communication or exchange of information, ideas, opinions and feedback with and among employees to collaborate in a work environment to achieve the desired results as set by the management and the employee.', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(5, 'Independent Work', 'The definition of independent is someone or something that is free from the influence or control of another. An example of independent is someone who lives on their own and supports themself.', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(6, 'Takes Initiative', 'Initiative is all about taking charge. An initiative is the first in a series of actions. Initiative can also mean a personal quality that shows a willingness to get things done and take responsibility. An initiative is the start of something, with the hope that it will continue.', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(9, 'Group Work', '', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(10, 'Productivity', '', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'admin', '2019-01-14 07:19:19', NULL),
(11, 'Creativity', '', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'admin', '2019-01-14 07:19:19', NULL),
(12, 'Honesty', '', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(13, 'Integrity', '', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'admin', '2019-01-14 07:19:19', NULL),
(14, 'Coworker Relations', '', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(15, 'Client Relations	', '', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(16, 'Technical Skills', '', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(17, 'Dependability', '', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(18, 'Punctuality', '', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL),
(19, 'Attendance', '', 'Unsatisfactory', 'Satisfactory', 'Good', 'Excellent', -2, 0, 1, 2, 'all', '2019-01-14 07:19:19', NULL);

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_01_14_050739_create_indicators_table', 1),
(4, '2019_01_14_065525_create_programs_table', 1),
(5, '2019_01_14_070257_create_evaluations_tabel', 1);

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `title`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'winter 2018', 'please all staffs do this', 'active', '2019-01-14 07:31:16', NULL),
(2, 'fall 2018', 'please all staffs do this', 'atop', '2019-01-14 07:31:16', NULL),
(3, 'summer 2018', 'please all staffs do this', 'active', '2019-01-14 07:31:16', NULL);

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `phone`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Navid', 'navidved@gmail.com', NULL, '$2y$10$WxAxFazYS0T0NKNZobPF.uZS/0ZsIGFc8Mwd6KUsw2K/Ivs4chUoG', NULL, 'staff', 'qNBESRtDYnNs6SL4lRWThM3NFCV1oreVdCZ70D8G2fiz40SRZtpfHqtKmozw', '2019-01-14 03:36:27', '2019-01-14 03:36:27'),
(2, 'Saber', 'admin@admin.com', NULL, '$2y$10$PTVbo.EuLUnXh3mXugFG8eQObyNCKdw7MWHq8tIZjbuFXEyPoPFtW', NULL, 'admin', 'GGU4cBoEIGoPPw0nUF5CxOCWUAxi7YgkFY2ZFtcKj5onTS5fyTcm2zfKEnbP', '2019-01-14 04:02:14', '2019-01-14 04:02:14'),
(3, 'Sam', 'sam@gmail.com', NULL, '$2y$10$WxAxFazYS0T0NKNZobPF.uZS/0ZsIGFc8Mwd6KUsw2K/Ivs4chUoG', NULL, 'staff', '9W5RrZqFVSV3PnzXaxcAyYldhE89yQCaWNoKKLtOA4SEcdPXNuSY8Eg8mV2X', '2019-01-14 03:36:27', '2019-01-14 03:36:27');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
