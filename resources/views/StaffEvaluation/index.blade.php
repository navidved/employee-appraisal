@extends('../layouts.home')

@section('card_title','Staff Evaluation')

@section('home_content')

    <div class="form-group">

        <p>
            Program title :
            <small class="text-muted">{{$ProgramInfo->title}}</small>
        </p>

        <p>
            Description :
            <small class="text-muted">{{$ProgramInfo->description}}</small>
        </p>

        <p>
            Staff :
            <small class="text-muted">{{$StaffInfo->name}}</small>
        </p>

    </div>

    <hr class="divider_line">

    <label>Indicators</label>

    <div class="accordion" id="IndicatorsAccordion">

        @foreach($IndicatorsWithAnswers as $Indicator)
            <div class="card">

                <div class="card-header" id="heading{{$Indicator->id}}">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse"
                                data-target="#collapse{{$Indicator->id}}"
                                aria-expanded="true" aria-controls="collapse{{$Indicator->id}}">
                            {{$Indicator->title}} @if($Indicator->user_access == 'admin') ( admin access only ) @endif
                        </button>
                    </h2>
                </div>

                <div id="collapse{{$Indicator->id}}" class="collapse @if ($loop->first) show @endif"
                     aria-labelledby="heading{{$Indicator->id}}"
                     data-parent="#IndicatorsAccordion">
                    <div class="card-body">
                        <p>
                            Description :
                            <small class="text-muted">{{$Indicator->description}}</small>
                        </p>

                        <hr class="divider_line">

                        <form method="POST" action="{{Request::fullUrl()}}">
                            @method('PATCH')
                            @csrf
                            <input type="hidden" name="indicator_id" value="{{$Indicator->id}}">

                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-2 pt-0">Options</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio"
                                                   name="selected_option" id="Option{{$Indicator->id}}" value="1">
                                            <label class="form-check-label" for="Option{{$Indicator->id}}">
                                                {{$Indicator->option_1}}
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio"
                                                   name="selected_option" id="Option{{$Indicator->id}}" value="2">
                                            <label class="form-check-label" for="Option{{$Indicator->id}}">
                                                {{$Indicator->option_2}}
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio"
                                                   name="selected_option" id="Option{{$Indicator->id}}" value="3">
                                            <label class="form-check-label" for="Option{{$Indicator->id}}">
                                                {{$Indicator->option_3}}
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio"
                                                   name="selected_option" id="Option{{$Indicator->id}}" value="4">
                                            <label class="form-check-label" for="Option{{$Indicator->id}}">
                                                {{$Indicator->option_4}}
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </fieldset>

                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
                </div>
            </div>

        @endforeach

    </div>

@endsection
