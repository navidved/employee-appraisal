@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">@yield('card_title')</div>

        <div class="card-body">
            @yield('home_content')
        </div>
    </div>

@endsection