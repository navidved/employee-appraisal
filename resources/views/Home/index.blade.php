@extends('../layouts.home')

@section('card_title','Dashboard')

@section('home_content')

    <p>Helper : Use <b>Evaluating</b> menu to start new staff evaluation or use <b>Results</b> menu to show evaluation results.</p>

@endsection
