@extends('../layouts.home')

@section('card_title','Evaluation Result')

@section('home_content')

    <div class="row">
        <div class="col-md-6">

            <form method="GET" action="/result">
                <div class="form-group">


                    <label for="Select_Program">Programs</label>
                    <select class="form-control" id="Select_Program" name="program_id" onchange="this.form.submit()">

                        <option disabled {{request()->has('program_id') ? '':'selected'}} value=""> -- select a program
                            --
                        </option>

                        @foreach($Programs as $program)

                            <option {{request('program_id') == $program->id  ? 'selected':''}}
                                    value="{{$program->id}}">{{$program->title}}</option>

                        @endforeach

                    </select>


                </div>

                @if(auth()->user()->role == 'admin')
                    <div class="form-group">

                        <label for="Select_Staff">Staffs</label>
                        <select class="form-control" id="Select_Staff" name="staff_id" onchange="this.form.submit()">

                            <option disabled {{request()->has('staff_id') ? '':'selected'}} value=""> -- select staff --
                            </option>

                            @foreach($Staffs as $staff)

                                <option {{request('staff_id') == $staff->id  ? 'selected':''}}
                                        value="{{$staff->id}}">{{$staff->name}}</option>

                            @endforeach

                        </select>

                    </div>
                @endif

            </form>

        </div>
        <div class="col-md-6">

            @if(request()->has('program_id'))

                <label>Results</label>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Indicator Title</th>
                        <th scope="col">Score</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($Results as $result)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{ $result->title }}</td>
                            <td>{{ $result->total_score }}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            @endif

        </div>
    </div>

@endsection