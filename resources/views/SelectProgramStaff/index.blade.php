@extends('../layouts.home')

@section('card_title','Selecting Evaluation Program and Staff')

@section('home_content')

    <div class="row">
        <div class="col-md-6">

            <div class="form-group">
                <form method="GET" action="/spf">

                    <label for="Select_Program">Active Programs</label>
                    <select class="form-control" id="Select_Program" name="program_id" onchange="this.form.submit()">

                        <option disabled {{request()->has('program_id') ? '':'selected'}} value=""> -- select a program
                            --
                        </option>

                        @foreach($ActivePrograms as $program)

                            <option {{request('program_id') == $program->id  ? 'selected':''}}
                                    value="{{$program->id}}">{{$program->title}}</option>

                        @endforeach

                    </select>

                </form>
            </div>

        </div>
        <div class="col-md-6">

            @if(request()->has('program_id'))

                <label>Staffs</label>

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Staff Name</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($Staffs as $staff)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{ $staff->name }}</td>
                            <td>
                                <a role="button" href="/stfEva/{{request('program_id')}}/{{$staff->id}}"
                                   class="btn btn-sm btn-info">Evaluation</a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            @endif

        </div>
    </div>



@endsection
